'use strict';


var dbConfig = function() {
  return {
    user: "dwh_tracabilite",
    password: "@trac@2021",
    server: "SRVDBBI",
    database: "DWH_Tracabilite",
    options: {
      /*connectTimeout: 1500000,
      requestTimeout: 1500000,*/
      trustServerCertificate: true,
    },
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 30000
    }
  };

};

module.exports = dbConfig();