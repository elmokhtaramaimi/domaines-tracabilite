'use strict';

var moment = require('moment');

var campaignConfig = function() {

  let year_now = moment().format("YYYY"),
    year_now_1 = parseInt(year_now) - 1,
    year_now_2 = parseInt(year_now) - 2;
  year_now = parseInt(year_now);

  return {
    campaign_date_start: [year_now_1, '1001'].join(''),
    campaign_date_end: [year_now, '0930'].join(''),
    campaign_1_date_start: [year_now_2, '1001'].join(''),
    campaign_1_date_end: [year_now_1, '0930'].join('')
  };

};

module.exports = campaignConfig();