/**  Main application file  **/

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
var path = require('path');
var bodyParser = require('body-parser');
var express = require('express');
var app = express()
const port = process.env.PORT || 5000


// parse application/json
app.use(bodyParser.json());                        
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

app.listen(port, () => {console.log(`Domaines Tracabilite app listening at http://localhost:${port}`)})
require(path.resolve('src/routes'))(app);




exports = module.exports = app;