const jwt = require('jsonwebtoken');

module.exports = function verifyJwtToken(req,res,next){
 /* #swagger.security = [{
    "bearerAuth": []
  }]
  */
 /* #swagger.responses[401] = {
      description: 'Unauthorized'
     } 
     #swagger.responses[400] = {
      description: 'Bad Request'
     } 
     #swagger.responses[500] = {
       description: 'Server Error',
     }
 */
 try {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      if (err) {
        console.log(err)
        return res.sendStatus(401);
      }else{
		  
	  console.log("token veriifed");
      req.user = user;
	  }
      next();
    });
 } catch (error) {
    console.log(error)
    next(error)
 }

}

