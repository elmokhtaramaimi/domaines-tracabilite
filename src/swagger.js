const swaggerAutogen = require('swagger-autogen')()
const doc = {
  info: {
    title: 'Domaines Tracabilite API',
  },
  host: process.env.host,
  schemes: ['http'],
  securityDefinitions: {
    bearerAuth: {
      type: 'apiKey',
      name: 'authorization',
      scheme: 'bearer',
      in: 'header',
    }
  }
};
const outputFile = 'src/swagger_output.json'
const endpointsFiles = ['src/routes.js']

swaggerAutogen(outputFile, endpointsFiles,doc)