path = require('path'),
  query = require(path.resolve('src/query'));
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));


exports.getProducteurs = async (req, res, next) => {
  try {
    let queryStrung = "SELECT DISTINCT FermeID,CodeProducteur, LibelleProducteur FROM AxeProducteur where FermeID is not null order by LibelleProducteur"
    let fermesData = await query.executeQuery(queryStrung)

    res.send(fermesData);
  } catch (error) {
    console.log(error)
    return next(error);
  }
}


exports.getCodesByFermeId = async (req, res, next) => {
  /*
      #swagger.parameters['fermeId'] = {
                in: 'url',
                description: 'fermeId',
                schema: number
          }
  */
  try {
    let fermeId = req.params.FermeId
    if (fermeId) {
      let queryString = `
      SELECT DISTINCT CodeTracabilite, AxeProducteur.FermeID, CodeProducteur FROM AxeParcelle,AxeProducteur
      WHERE AxeParcelle.FermeID=AxeProducteur.FermeId
      AND AxeParcelle.FermeId = ${fermeId}
      `
      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("femreId required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}