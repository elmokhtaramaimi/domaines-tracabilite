path = require('path'),
  query = require(path.resolve('src/query'));
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));

exports.getParcellesByFermeIdAndCodeTracabilite = async (req, res, next) => {



  /*
      #swagger.parameters['FermeId'] = {
                in: 'url',
                description: 'fermeId',
                schema: number
          }
      #swagger.parameters['CodeTracabilite'] = {
                in: 'url',
                description: 'CodeTracabilite',
                schema: string
          }
  */
  try {
    if (req.params.FermeId && req.params.CodeTracabilite) {
      let fermeId = req.params.FermeId
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `
        SELECT dateArrachage, NumParcelle,Libellevariete,PorteGreffe,round(Superficie,4) as Superficie,DateInstallation,Ecartement,SurgreffeePlantee,NbrArbres FROM AxeParcelle
        INNER JOIN AxeVarieteLogista ON AxeVarieteLogista.VarieteID = AxeParcelle.VarieteId
        WHERE
  	  (CodeTracabilite='${codeTracabilite}'
        AND FermeID=${fermeId})

  	  and (
  	  dateArrachage  >= '${get_agricultural_campaign.campaign_date_start}'
  	  or (dateArrachage is null)
  	  )
      `
      console.log(queryString);
      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("FermeId And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}

exports.getParcellesIrrigationByFermeIdAndCodeTracabilite = async (req, res, next) => {
  /*
    #swagger.parameters['FermeId'] = {
              in: 'url',
              description: 'fermeId',
              schema: number
        }
    #swagger.parameters['CodeTracabilite'] = {
              in: 'url',
              description: 'CodeTracabilite',
              schema: string
        }
*/
  try {
    if (req.params.FermeId && req.params.CodeTracabilite) {
      let fermeId = req.params.FermeId
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `
      SELECT
        AxeParcelle.NumParcelle,
       (SELECT
       round(sum(QteEauM3/nullif(Superficie,0)),2)
       FROM FactCTIrrigation
       WHERE FactCTIrrigation.ParcelleCulturaleId in
          ( SELECT ParcelleCulturaleId FROM AxeParcelle
            WHERE CodeTracabilite='${codeTracabilite}'
            AND FermeID=${fermeId}

          )
        AND (DateId between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}')
      	and FactCTIrrigation.ParcelleCulturaleId = FactCTFertilisation.ParcelleCulturaleId
      GROUP BY  FactCTIrrigation.ParcelleCulturaleId) AS 'Eau / ha (m3)' ,
       round(Sum(N),2) as 'N',round(Sum(P),2) as 'P',round(Sum(K),2) as 'K',
       round((Sum(N/nullif(Sup,0))),2) as 'N/ha',
       round((Sum(P/nullif(Sup,0))),2) as 'P/ha',
       round((Sum(K/nullif(Sup,0))),2) as 'K/ha'
       FROM FactCTFertilisation
        LEFT JOIN AxeParcelle ON AxeParcelle.ParcelleCulturaleId = FactCTFertilisation.ParcelleCulturaleId
          WHERE

      	 FactCTFertilisation.ParcelleCulturaleId in
          ( SELECT ParcelleCulturaleId FROM AxeParcelle
            WHERE CodeTracabilite='${codeTracabilite}'
            AND FermeID=${fermeId}

          )
          and (dateId  between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}' )
          GROUP BY AxeParcelle.NumParcelle,FactCTFertilisation.ParcelleCulturaleId
    `

      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("FermeId And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}