path = require('path'),
  query = require(path.resolve('src/query'));
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));

exports.getTraitement = async (req, res, next) => {

  try {
    if (req.params.FermeId && req.params.CodeTracabilite) {
      let fermeId = req.params.FermeId
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `select
      FactCTTraitementPhyto.DateId as date,
      AxeParcelle.NumParcelle,
        FactCTTraitementPhyto.cible as motif,
        FactCTTraitementPhyto.MatiereActive,
        FactCTTraitementPhyto.Dose,
        FactCTTraitementPhyto.DAR,
        FactCTTraitementPhyto.Quantite
        from FactCTTraitementPhyto, AxeParcelle
        where
        FactCTTraitementPhyto.ParcelleCulturaleId = AxeParcelle.ParcelleCulturaleId
      AND FactCTTraitementPhyto.FermeId = ${fermeId}
      AND AxeParcelle.CodeTracabilite = '${codeTracabilite}'
      AND (FactCTTraitementPhyto.DateId between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}')
      order by 
	    FactCTTraitementPhyto.DateId,
	    AxeParcelle.NumParcelle`
      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("FermeId And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}

exports.getParcellesPhytoByFermeIdAndCodeTracabilite = async (req, res, next) => {

  try {
    if (req.params.FermeId && req.params.CodeTracabilite) {
      let fermeId = req.params.FermeId
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `

      --get this out => Parcelle, Date traitement, Catégories, Produits, refordre, boullie, quantite
      from

      select *
      (select *
      from FactCTTraitementPhyto
      where FactCTTraitementPhyto.FermeId = 1) as T1,

      (select
      T.ParcelleCulturaleId,
      T.NumParcelle,
      T.DateId
      from
      (
      select
      FactCTTraitementPhyto.ParcelleCulturaleId,
      FactCTTraitementPhyto.NumParcelle,
      FactCTTraitementPhyto.DateId,
      row_number() over(partition by FactCTTraitementPhyto.ParcelleCulturaleId order by FactCTTraitementPhyto.DateId desc) as rn
      from
      FactCTTraitementPhyto
      where
      FactCTTraitementPhyto.FermeId = 1
      --AND (FactCTTraitementPhyto.DateId between '' and '')
      ) as T
      where T.rn = 1) as T2

      where T1.ParcelleCulturaleId = T2.ParcelleCulturaleId
      and T1.DateId = T2.DateId
    `
      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("FermeId And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}