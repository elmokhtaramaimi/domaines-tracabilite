require('dotenv').config();
var jwt = require('jsonwebtoken');
const ActiveDirectory = require('activedirectory');

const path = require('path');
const active_directory_config = require(path.resolve('src/config/active_directory_config'));
const suffix = active_directory_config.suffix;

const ad = new ActiveDirectory(active_directory_config);

exports.login = (req, res, next) => {
  /*
  #swagger.tags = ['Auth']
  #swagger.description = 'Endpoint used to authenticate a user .'
  #swagger.parameters['username'] = {
                in: 'body',
                description: 'username / password',
                 schema: {
                    username: 'hhnida',
                    password: 'h@ss@n21',
                }
          }
  */

  var {
    username,
    password
  } = req.body
  username = username + suffix;
  try {
    if (username && password) {
      // Authenticate the user
      ad.authenticate(username, password, function(err, auth) {
        if (err) {
          console.log(err);
          return res.status(500).send(err);
        }
        if (auth) {
          // Generating JWT
          let user = {
            username: username
          }
          var token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '3d'
          });
          var data = {
            accessToken: token
          }
          console.log(token);
          return res.send(data)
        } else {
          /* #swagger.responses[401] = {
               description: 'Authentication failed',
           } */
          console.log('Authentication failed!');
          return res.status(401);
        }
      });
    } else {
      return res.status(400).send("username and password both are required");
    }
  } catch (error) {
    /* #swagger.responses[500] = {
        description: 'Server Error',
    } */
    console.log(error);
    return next(error);
  }
}


exports.logout = (req, res) => {
  /*
  #swagger.tags = ['Auth']
  */
  return res.status(200).send(true)
}