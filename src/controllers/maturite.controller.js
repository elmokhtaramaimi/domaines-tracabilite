path = require('path'),
  query = require(path.resolve('src/query'));
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));


exports.getMaturiteByCProAndCTrac = async (req, res, next) => {
  /*
      #swagger.parameters['CodeProducteur'] = {
                in: 'url',
                description: 'CodeProducteur',
                schema: number
          }
      #swagger.parameters['CodeTracabilite'] = {
                in: 'url',
                description: 'CodeTracabilite',
                schema: string
          }
  */
  try {
    if (req.params.CodeProducteur && req.params.CodeTracabilite) {
      let codeProducteur = req.params.CodeProducteur
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `
      SELECT

          axst.CodeSociete as LibelleSociete,
          round(avg(Brix),2) as brix,
          round(avg(Acidite),2) as acidite,
          round(avg(EA),2) as ea,
          round(avg(PercentJus),2) as PercentJus,
          round(avg(TauxFruitsAPepins),2) as TauxFruitsAPepins

          FROM FactTRMaturite ftm
          ​
          LEFT JOIN FactTRVersementReception ftr ON (ftm.NumBR = ftr.NumBR  AND ftm.CodeProducteur = ftr.CodeProducteur)
          ​
          LEFT JOIN AxeStation axst ON ftm.CodeStation = axst.SocieteId
          ​
          WHERE ftr.CodeTracabilité = '${codeTracabilite}'
          ​
          AND ftm.CodeProducteur = ${codeProducteur}
          ​
          AND ftm.DateReception between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}'
          ​
          GROUP BY axst.CodeSociete
      `
      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("CodeProducteur And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}