path = require('path'),
  query = require(path.resolve('src/query')),
  lodash = require('lodash');
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));
exports.getEcartsByCProAndCTrac = async (req, res, next) => {
  /*
      #swagger.parameters['CodeProducteur'] = {
                in: 'url',
                description: 'CodeProducteur',
                schema: number
          }
      #swagger.parameters['CodeTracabilite'] = {
                in: 'url',
                description: 'CodeTracabilite',
                schema: string
          }
  */
  try {
    if (req.params.CodeProducteur && req.params.CodeTracabilite) {
      let codeProducteur = req.params.CodeProducteur
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `SELECT
​
        LibelleSociete as societe,
        CategorieDefaut as categorie,
        LibelleDefaut as souscategorie,
        FORMAT(round(sum(value),4),'P') as value FROM (
        ​
        SELECT
        ​
        	P.CodeStation as LibelleSociete,
        	P.CategorieDefaut,
        	P.LibelleDefaut,
        	pourcentage,
        	Pourcentage / sum(Pourcentage) OVER (PARTITION BY P.CodeStation) as value

        FROM FactTRAgreageEcartReception P

        LEFT JOIN FactTRVersementReception ftr ON (P.NumBR = ftr.NumBR  AND P.CodeProducteur = ftr.CodeProducteur)
        ​
        LEFT JOIN AxeStation PA ON P.CodeStation = PA.CodeSociete
        ​
        WHERE ftr.CodeTracabilité = '${codeTracabilite}'
        ​
        AND P.CodeProducteur =  ${codeProducteur}
        ​
        AND P.DateReception between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}') as t
        ​
        GROUP BY LibelleSociete, CategorieDefaut, LibelleDefaut
      `;
      let data = await query.executeQuery(queryString)
      let stations = [];

      data.forEach((c) => {
        if (!stations.includes(c.societe)) stations.push(c.societe)
      });


      // grouping data by categories
      let groupedcategories = lodash.groupBy(data, function(categorie) {
        return categorie.categorie;
      })



      /*lodash.forEach(groupedcategories, function(value, key) {
        groupedcategories[key] = lodash.groupBy(groupedcategories[key], function(item) {
          return item.LibelleDefaut;
        });
      });*/



      // mapping grouped categories
      let mapedcategories = lodash.map(groupedcategories, (objs, key) => {
        return Object.assign({
          categorie: key
        }, ...objs.map((obj) => {
          return {
            [obj.societe]: obj.value
          }
        }))
      });


      // adding non exite val with 0 value
      let categories = lodash.map(mapedcategories, (stationData) => {
        let data = stationData
        stations.forEach((station) => {
          if (stationData[station] == undefined) data = {
            ...data,
            [station]: 0
          }
        });
        return data
      });

      categories = groupedcategories;
      res.send({
        categories,
        stations: stations
      });

      /*  console.log({
          categories,
          stations: stations
        });*/

      /*
        // setting categories for the Header
        let categories = [];
        categories = lodash.groupBy(data, function(station) {
          return station.CategorieDefaut;
        })
        categories = lodash.map(categories, (objs, key) => {
          return {
            categorie: key,
            sous_categories: lodash.uniq(objs.map((obj) => obj.LibelleDefaut))
          }
        });

        let sousCategories = [];
        categories.forEach((categorie) => {
          sousCategories = sousCategories.concat(categorie.sous_categories)
        });
        // setting data by station
        let stations = []
        stations = lodash.groupBy(data, function(station) {
          return station.LibelleSociete;
        })
        stations = lodash.map(stations, (objs, key) => {
          return Object.assign({
            station: key
          }, ...objs.map((obj) => {
            return {
              [obj.LibelleDefaut]: obj.Percentage
            }
          }))
        });
        stations = lodash.map(stations, (stationData) => {
          let data = stationData
          sousCategories.forEach((sousCategorie) => {
            if (stationData[sousCategorie] == undefined) data = {
              ...data,
              [sousCategorie]: 0
            }
          });
          return data
        });*/
      /* stations.forEach(stationData => {
        console.log(stationData.station)
        categories.forEach(categorieData => {
          console.log("- " +categorieData.categorie)
          categorieData.sous_categories.forEach(sousCategorie => {
            console.log("----- "+sousCategorie+" : "+stationData[sousCategorie])
          });
        });
      }); */

    } else {
      res.status(403).send("CodeProducteur And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}