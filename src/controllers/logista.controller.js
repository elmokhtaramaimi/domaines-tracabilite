path = require('path'),
  query = require(path.resolve('src/query'));
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));

exports.getLogistaReceByCProAndCTrac = async (req, res, next) => {

  try {
    if (req.params.CodeProducteur && req.params.CodeTracabilite) {
      let codeProducteur = req.params.CodeProducteur
      let codeTracabilite = req.params.CodeTracabilite
      let queryString = `
        SELECT CodeStation, DateReception, NumBR, NumBL, PrgSpecial, round(PoidsReception,4) as PoidsReception
        FROM FactTRVersementReception
        WHERE CodeTracabilité = '${codeTracabilite}'
        AND CodeProducteur = ${codeProducteur}
        AND (DateReception between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}')
      `

      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("CodeProducteur And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}

exports.getLogistaExpeByCodeTracabilite = async (req, res, next) => {
  /*
    #swagger.parameters['CodeTracabilite'] = {
              in: 'url',
              description: 'CodeTracabilite',
              schema: string
        }
*/
  try {
    if (req.params.CodeTracabilite) {
      let codeTracabilite = req.params.CodeTracabilite
      let codeProducteur = req.params.CodeProducteur
      let queryString = `SELECT      ​
      expr.codeproducteur,
        expr.CodeStation,
        expr.NumVersement,
        expr.NumDossier,
        expr.DateDEpart,
        expr.NumPalette,
        expr.PortDepart,
        expr.PortArrive,
		    round(Sum(expr.TON_EXP_PDS_COM),2) as 'TON_EXP_PDS_COM',
		   round(Sum(expr.TON_EXP_PDS_REEL),2) as 'TON_EXP_PDS_REEL'    ​
      FROM FactTRVersementExport expr      ​
      LEFT JOIN FactTRVersementReception recp ON (recp.NumVersement = expr.NumVersement) AND (recp.CodeProducteur = expr.CodeProducteur)      ​
      WHERE expr.CodeProducteur = '${codeTracabilite}' AND  ​
       recp.CodeTracabilité = '${codeProducteur}'      ​
      AND recp.DateReception between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}'
      GROUP BY
       expr.codeproducteur,
       expr.CodeStation,
       expr.NumVersement,
       expr.NumDossier,
       expr.DateDEpart,
       expr.NumPalette,
       expr.PortDepart,
       expr.PortArrive
       ORDER BY expr.DateDEpart DESC
    `

      //  console.log(queryString);
      let data = await query.executeQuery(queryString)
      res.send(data);
    } else {
      res.status(403).send("CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}