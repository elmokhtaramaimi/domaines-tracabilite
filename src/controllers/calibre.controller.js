path = require('path'),
  query = require(path.resolve('src/query')),
  lodash = require('lodash');
const get_agricultural_campaign = require(path.resolve('src/config/get_agricultural_campaign'));
exports.getCalibreByCProAndCTrac = async (req, res, next) => {
  /*
      #swagger.parameters['CodeProducteur'] = {
                in: 'url',
                description: 'CodeProducteur',
                schema: number
          }
      #swagger.parameters['CodeTracabilite'] = {
                in: 'url',
                description: 'CodeTracabilite',
                schema: string
          }
  */
  try {
    if (req.params.CodeProducteur && req.params.CodeTracabilite) {
      let codeProducteur = req.params.CodeProducteur
      let codeTracabilite = req.params.CodeTracabilite
      /*  let queryString = `
          SELECT LibelleSociete, Calibre, round(avg(Value),2) as value FROM FactTRCalibre, AxeStation
          WHERE CodeStation = CodeSociete
          AND NumBR in
          (
          SELECT NumBR
          FROM FactTRVersementReception
          WHERE CodeTracabilité = '${codeTracabilite}'
          AND CodeProducteur = ${codeProducteur}
          )
          Group by LibelleSociete,Calibre
        `  */
      let queryString = `SELECT CodeStation as LibelleSociete, Calibre, FORMAT(value/sum(value) OVER (PARTITION BY CodeStation),'P') as value FROM (
​
              SELECT
              ​
              	ftc.CodeStation,
              	Calibre,
              	avg(Value) as value
              ​
              FROM FactTRCalibre ftc
              ​
                  LEFT JOIN FactTRVersementReception ftr ON (ftc.NumBR = ftr.NumBR  AND ftc.CodeProducteur = ftr.CodeProducteur)
              ​
              WHERE ftr.CodeTracabilité = '${codeTracabilite}'
              ​
              AND ftc.CodeProducteur = ${codeProducteur}
              ​
              AND ftc.DateReception  between '${get_agricultural_campaign.campaign_1_date_start}' and '${get_agricultural_campaign.campaign_1_date_end}'
              ​
              GROUP BY ftc.CodeStation, Calibre) as t


              `;

      let data = await query.executeQuery(queryString)
      let calibres = [];
      // getting calibers without duplicates
      data.forEach((c) => {
        if (!calibres.includes(c.Calibre)) calibres.push(c.Calibre)
      });
      // grouping data by station
      let groupedStations = lodash.groupBy(data, function(station) {
        return station.LibelleSociete;
      })
      // mapping grouped stations
      let mapedStations = lodash.map(groupedStations, (objs, key) => {
        return Object.assign({
          station: key
        }, ...objs.map((obj) => {
          return {
            [obj.Calibre]: obj.value
          }
        }))
      });
      // adding non exite calibre with 0 value
      let stations = lodash.map(mapedStations, (stationData) => {
        let data = stationData
        calibres.forEach((calibre) => {
          if (stationData[calibre] == undefined) data = {
            ...data,
            [calibre]: 0
          }
        });
        return data
      });

      console.log({
        calibres,
        stations: stations
      });
      res.send({
        calibres,
        stations: stations
      });
    } else {
      res.status(403).send("CodeProducteur And CodeTracabilite required");
    }
  } catch (error) {
    console.log(error)
    return next(error);
  }
}