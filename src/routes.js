// /* Main App Routes */
"use strict";

const cors = require('cors');
const appEndPoint = "/api";
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
const verifyJwtToken = require('./middlewares/authentication.middleware.js')


const authRoutes = require('./routes/auth.router');
const producteurRoutes = require('./routes/producteur.router');
const parcellesRoutes = require('./routes/parcelle.router');
const logistaRoutes = require('./routes/logista.router');
const maturiteRoutes = require('./routes/maturite.router');
const calibreRoutes = require('./routes/calibre.router');
const ecartsRoutes = require('./routes/ecarts.router');
const traitementRoutes = require('./routes/traitementphyto.router');
module.exports = function(app) {
  app.use(function(req, res, next) {
    console.log('Request URL:', req.originalUrl)
    next()
  }, )
  var corsOptions = {
    // origin: config.app.frontend_url, //'http://localhost:3000',
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  };
  app.use('/*', cors(corsOptions));

  /* App global endPointes */
  app.get("/", (req, res) => {
    res.sendFile(__dirname + "/views/index.html");
  });
  // swagger documentation endPoint
  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))
  // auth endPoint
  app.use('/auth', authRoutes);
  // apply authentication middelware on all api's
  app.use(appEndPoint + '/producteurs', verifyJwtToken, producteurRoutes);
  app.use(appEndPoint + '/parcelles', verifyJwtToken, parcellesRoutes);
  app.use(appEndPoint + '/logista', verifyJwtToken, logistaRoutes);
  app.use(appEndPoint + '/maturite', verifyJwtToken, maturiteRoutes);
  app.use(appEndPoint + '/calibre', verifyJwtToken, calibreRoutes);
  app.use(appEndPoint + '/ecarts', verifyJwtToken, ecartsRoutes);
  app.use(appEndPoint + '/traitementphyto', verifyJwtToken, traitementRoutes);

};