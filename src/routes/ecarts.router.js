var express = require("express");
var path = require("path");
var ecartsController = require(path.resolve('src/controllers/ecarts.controller'));


var router = express.Router();

router.get('/:CodeProducteur/:CodeTracabilite' ,
  //#swagger.tags = ['Ecarts']  
  //#swagger.description = 'Endpoint returns  Causes écarts  Data  filtred By CodeProducteur  (ex:7272) And CodeTracabilite  (ex:TR21081-7272-185)  .'   

  ecartsController.getEcartsByCProAndCTrac
);




module.exports = router;