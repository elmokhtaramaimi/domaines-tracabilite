var express = require("express");
var path = require("path");
var calibreController = require(path.resolve('src/controllers/calibre.controller'));


var router = express.Router();

router.get('/:CodeProducteur/:CodeTracabilite' ,
  //#swagger.tags = ['Calibre']  
  //#swagger.description = 'Endpoint returns Profil calibre  Data  filtred By CodeProducteur  (ex:7272) And CodeTracabilite  (ex:TR21081-7272-185)  .'   

  calibreController.getCalibreByCProAndCTrac
);




module.exports = router;