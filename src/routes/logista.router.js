var express = require("express");
var path = require("path");
var logistaController = require(path.resolve('src/controllers/logista.controller'));


var router = express.Router();

router.get('/reception/:CodeProducteur/:CodeTracabilite',
  //#swagger.tags = ['Logista']
  //#swagger.description = 'Endpoint returns Logista-Réception  Data  filtred By CodeProducteur  (ex:7272) And CodeTracabilite  (ex:TR21081-7272-185)  .'
  logistaController.getLogistaReceByCProAndCTrac
);
router.get('/export/:CodeProducteur/:CodeTracabilite',
  //#swagger.tags = ['Logista']
  //#swagger.description = 'Endpoint returns Logista-Export Data  filtred By  CodeTracabilite  (ex:TR21081-7272-185)'
  logistaController.getLogistaExpeByCodeTracabilite
);



module.exports = router;