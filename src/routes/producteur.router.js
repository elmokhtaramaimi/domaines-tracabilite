var express = require("express");
var path = require("path");
var producteurController = require(path.resolve('src/controllers/producteur.controller'));


var router = express.Router();

router.get('/' ,
//#swagger.tags = ['Producteur']  
producteurController.getProducteurs
); 
router.get('/Codes/:FermeId',
//#swagger.tags = ['Producteur']  
//#swagger.description = 'Endpoint returns code Tracabilite filtred by fermeID (ex:16)  .'   
producteurController.getCodesByFermeId
);


module.exports = router;