var express = require("express");
var path = require("path");
var traitementphytoController = require(path.resolve('src/controllers/traitementphyto.controller'));


var router = express.Router();

router.get('/:FermeId/:CodeTracabilite',
  //#swagger.tags = ['Parcelle']
  //#swagger.description = 'Endpoint returns parcelles filtred By FermeId  (ex:16) And CodeTracabilite  (ex:TR50000)  .'
  traitementphytoController.getTraitement
);
router.get('/traitementphyto/:FermeId/:CodeTracabilite',
  //#swagger.tags = ['Parcelle']
  //#swagger.description = 'Endpoint returns parcelles traitement phyto filtred By FermeId  (ex:16) And CodeTracabilite  (ex:TR50000)  .'
  traitementphytoController.getParcellesPhytoByFermeIdAndCodeTracabilite
);




module.exports = router;