var express = require("express");
var path = require("path");
var maturiteController = require(path.resolve('src/controllers/maturite.controller'));


var router = express.Router();

router.get('/:CodeProducteur/:CodeTracabilite' ,
  //#swagger.tags = ['Maturité']  
  //#swagger.description = 'Endpoint returns Maturité Data  filtred By CodeProducteur  (ex:7272) And CodeTracabilite  (ex:TR21081-7272-185)  .'   
  maturiteController.getMaturiteByCProAndCTrac
);




module.exports = router;