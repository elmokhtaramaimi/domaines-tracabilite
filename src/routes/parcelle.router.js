var express = require("express");
var path = require("path");
var parcelleController = require(path.resolve('src/controllers/parcelle.controller'));


var router = express.Router();

router.get('/:FermeId/:CodeTracabilite' ,
  //#swagger.tags = ['Parcelle']  
  //#swagger.description = 'Endpoint returns parcelles filtred By FermeId  (ex:16) And CodeTracabilite  (ex:TR50000)  .'   
  parcelleController.getParcellesByFermeIdAndCodeTracabilite
);
router.get('/irrigation/:FermeId/:CodeTracabilite' ,
  //#swagger.tags = ['Parcelle']  
  //#swagger.description = 'Endpoint returns parcelles irrigation filtred By FermeId  (ex:16) And CodeTracabilite  (ex:TR50000)  .'   
  parcelleController.getParcellesIrrigationByFermeIdAndCodeTracabilite
); 



module.exports = router;