var express = require("express");
var path = require("path");
var authController = require('../controllers/auth.controller');


var router = express.Router();


router.post('/login' , authController.login);
/* router.get('/logout' , authController.logout); */


module.exports = router;