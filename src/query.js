/**
 * Initialize global configuration files
 */
var sql = require("mssql"),
  path = require('path'),
  dbConfig = require(path.resolve('src/config/db_config'));


var initQuery = function() {

  var config = {};
  config.production = true;
  config.db_name = dbConfig.database;


  config.executeQuery = async function(query) {
    dbConfig.connectionTimeout = 0;
    dbConfig.requestTimeout = 0;
    /*  dbConfig.pool.acquireTimeoutMillis = 0;
      dbConfig.pool.idleTimeoutMillis = 0;*/
    var dbConn = new sql.ConnectionPool(dbConfig);
    //  console.log(dbConn);
    await dbConn.connect();
    let result = await new sql.Request(dbConn).query(query);

    return result.recordsets[0];
  }

  return config
};

/**
 * Set configuration object
 */
module.exports = initQuery();